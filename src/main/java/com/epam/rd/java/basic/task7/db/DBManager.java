package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.mysql.cj.Messages;
import com.mysql.cj.jdbc.JdbcStatement;

import javax.sql.DataSource;


public class DBManager {

    private static DBManager instance;


    public static synchronized DBManager getInstance() {
        if (instance == null) instance = new DBManager();
        return instance;
    }

    private DBManager() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public Connection getConnection() throws DBException {
        Connection connection = null;
        try {
            String connectionUrl = getConnectionUrl();
            connection = DriverManager.getConnection(connectionUrl);

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return connection;
    }

    public List<User> findAllUsers() throws DBException {

        String sql = "SELECT * FROM users";
        List<User> users = new ArrayList<>();


        try (
                Connection connection = getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(sql);
        ) {
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                users.add(user);
            }

        } catch (SQLException ignored) {

        }
        return users;


    }

    private static <T, U extends Comparable<? super U>> List<T>
    sort(List<T> items, Function<T, U> extractor) {
        items.sort(Comparator.comparing(extractor));
        return items;
    }


    public static void main(String[] args) throws DBException {
        DBManager dbm = DBManager.getInstance();
        List<User> users = IntStream.range(1, 5)
                .mapToObj(x -> "user" + x)
                .map(User::createUser)
                .collect(Collectors.toList());

        for (User user : users) {
            dbm.insertUser(user);
        }

        List<User> usersFromDB = sort(dbm.findAllUsers(), User::getLogin);
        System.out.println(usersFromDB);
        System.out.println(users);


    }


    public boolean insertUser(User user) throws DBException {
        if (user == null) return false;
        String sql = "INSERT INTO users(login) VALUES(?)";
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ) {
            preparedStatement.setString(1, user.getLogin());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) return false;

            try (ResultSet generatedKeysResult = preparedStatement.getGeneratedKeys()) {
                if (generatedKeysResult.next()) user.setId(generatedKeysResult.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }


    public boolean deleteUsers(User... users) throws DBException {
        String sql = "DELETE FROM users WHERE id = ?";
        int deletedCount = 0;
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            for (User u : users) {
                preparedStatement.setInt(1, u.getId());
                deletedCount = deletedCount + preparedStatement.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return deletedCount != 0;
    }


    public User getUser(String login) throws DBException {
        String sql = "SELECT * FROM users WHERE login=?";
        User fetchedUser = new User();
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                fetchedUser.setLogin(resultSet.getString(2));
                fetchedUser.setId(resultSet.getInt(1));
            }

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return fetchedUser;

    }


    public Team getTeam(String name) throws DBException {
        String sql = "SELECT * FROM teams WHERE name=?";
        Team fetchedTeam = new Team();
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                fetchedTeam.setName(resultSet.getString(2));
                fetchedTeam.setId(resultSet.getInt(1));
            }

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return fetchedTeam;
    }

    public List<Team> findAllTeams() throws DBException {
        String sql = "SELECT * FROM teams";
        List<Team> teams = new ArrayList<>();


        try (
                Connection connection = getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(sql);
        ) {
            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt(1));
                team.setName(resultSet.getString(2));
                teams.add(team);
            }

        } catch (SQLException ignored) {

        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        if (team == null) return false;
        String sql = "INSERT INTO teams(name) VALUES(?)";
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ) {
            preparedStatement.setString(1, team.getName());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) return false;

            try (ResultSet generatedKeysResult = preparedStatement.getGeneratedKeys()) {
                if (generatedKeysResult.next()) team.setId(generatedKeysResult.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        String sql = "INSERT INTO users_teams values(?,?)";
        int affectedRows = 0;
        Connection connection = getConnection();


        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql);

        ) {
            connection.setAutoCommit(false);
            preparedStatement.setInt(1, user.getId());

            for (Team team : teams) {
                preparedStatement.setInt(2, team.getId());
                affectedRows += preparedStatement.executeUpdate();
            }
            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new DBException(e.getMessage(),e);

        }
        return affectedRows != 0;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        String sql =
                "SELECT * FROM teams \n" +
                        "LEFT JOIN users_teams ON users_teams.team_id = teams.id\n" +
                        "LEFT JOIN users ON users.id = users_teams.user_id\n" +
                        "WHERE users.id = ?";

        List<Team> userTeams = new ArrayList<>();
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                userTeams.add(team);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userTeams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String sql = "DELETE FROM teams WHERE id = ?";
        int deletedCount = 0;
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1, team.getId());
            deletedCount = preparedStatement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return deletedCount != 0;
    }

    public boolean updateTeam(Team team) throws DBException {
        int affectedRows = 0;
        String sql = "UPDATE teams SET name=? WHERE id=?";
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql);

        ) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, team.getId());
            affectedRows = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return affectedRows != 0;
    }

    private String getConnectionUrl() {
        Properties properties = new Properties();
        try (InputStream stream = Files.newInputStream(Paths.get("app.properties"))) {
            properties.load(stream);
        } catch (IOException ignored) {
        }
        return properties.getProperty("connection.url");
    }

}

/*
package com.epam.rd.java.basic.task7.db;
 

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;
 

public class DBManager{

	private static DBManager instance;
	
	private final static String URL = "jdbc:mysql://localhost:3306/test2db";
	private final static String USER = "root";
	private final static String PASSWORD = "root";
	private final static String DB_URL = getConnectionUrl();
	
	private static String getConnectionUrl() {
        Properties properties = new Properties();
        try (InputStream stream = Files.newInputStream(Paths.get("app.properties"))) {
            properties.load(stream);
        } catch (IOException ignored) {
        }
        return properties.getProperty("connection.url");
	}    
	
	
	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> allUsers = new ArrayList();

		try(Connection connection = DriverManager.getConnection(DB_URL); 
				Statement statement = connection.createStatement(); 
				ResultSet rs = statement.executeQuery("SELECT id, login FROM users ORDER BY login;");) {
			
			while(rs.next()) {
				User usr = User.createUser(rs.getString("login"));
				usr.setId(rs.getInt("id"));
				allUsers.add(usr);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			//throw new DBException("findAllUsers(): failed to get list of users", e);
		}
		
		return allUsers;
	}

	public boolean insertUser(User user) throws DBException {
		if (user != null) {
			try (Connection connection = DriverManager.getConnection(DB_URL); 
					PreparedStatement prst = connection.prepareStatement("INSERT INTO users (id, login) VALUES (?, ?);", Statement.RETURN_GENERATED_KEYS)){
				
				prst.setInt(1, user.getId());
				prst.setString(2, user.getLogin());

				if (prst.executeUpdate() == 1) {
						return true;
				}
				
			
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DBException("insertUser(): failed insertion", e);
			}
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		if (users.length == 0) {
			return false;		
		}
		Connection con = null;	
		try {
			con = DriverManager.getConnection(DB_URL);
			con.setAutoCommit(false);
			
			for (User u : users) {
				if (u != null) {
					deleteUser(con, u.getId());
				}
			}
			con.commit();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			rollBack(con);
			throw new DBException("setTeamsForUser(): failed ", e);
			//return false;
		} finally {
			turnOnAutoCommit(con);
			close(con);
		}
	}

	public User getUser(String login) throws DBException {
		User user = null;
		try(Connection connection = DriverManager.getConnection(DB_URL); 
				Statement statement = connection.createStatement(); 
				ResultSet rs = statement.executeQuery("SELECT id, login FROM users WHERE login = '" + login + "';");) {
			
			if(rs.next()) {
				user = User.createUser(rs.getString("login"));
				user.setId(rs.getInt("id"));
			} else {
				throw new SQLException();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			//throw new DBException("getUser(): failed to get a user", e);
		}
		
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		try(Connection connection = DriverManager.getConnection(DB_URL); 
				Statement statement = connection.createStatement(); 
				ResultSet rs = statement.executeQuery("SELECT id, name FROM teams WHERE name = '" + name + "';");) {
			
			if(rs.next()) {
				team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("id"));
			} else {
				throw new SQLException();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("getUser(): failed to get a user", e);
		}
		
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> allTeams = new ArrayList();

		try(Connection connection = DriverManager.getConnection(DB_URL); Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery("SELECT id, name FROM teams ORDER BY name;");
			while(rs.next()) {
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("id"));
				allTeams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("findAllTeams(): failed to get list of teams", e);
		}
		
		return allTeams;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (team != null) {
			try (Connection connection = DriverManager.getConnection(DB_URL); 
					PreparedStatement prst = connection.prepareStatement("INSERT INTO teams (id, name) VALUES (?, ?);", Statement.RETURN_GENERATED_KEYS)){
				
				prst.setInt(1, team.getId());
				prst.setString(2, team.getName());

				if (prst.executeUpdate() == 1) {
						return true;
				}
				
			
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DBException("insertUser(): failed insertion", e);
			}
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null || teams.length == 0) {
			return false;		
		}
		Connection con = null;	
		try {
			con = DriverManager.getConnection(DB_URL);
			con.setAutoCommit(false);
			
			for (Team t : teams) {
				if (t != null) {
					addTeamForUser(con, user.getId(), t.getId());
				}
			}
			con.commit();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			rollBack(con);
			throw new DBException("setTeamsForUser(): failed ", e);
			//return false;
		} finally {
			turnOnAutoCommit(con);
			close(con);
		}
	}


	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList();

		try(Connection connection = DriverManager.getConnection(DB_URL); Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery("SELECT team_id, teams.name FROM users_teams JOIN teams ON team_id = teams.id WHERE user_id = " + user.getId() + ";");
			while(rs.next()) {
				Team team = Team.createTeam(rs.getString("name"));
				team.setId(rs.getInt("team_id"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("getUserTeams(): failed ", e);
		}
		
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		if (team != null) {
			try (Connection connection = DriverManager.getConnection(DB_URL); Statement statement = connection.createStatement()){
				
				return statement.executeUpdate("DELETE FROM teams WHERE teams.name = '" + team.getName() + "';") > 0;
			
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DBException("deleteTeam(): failed ", e);
			}
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		if (team != null) {
			try (Connection connection = DriverManager.getConnection(DB_URL); Statement statement = connection.createStatement()){
				
				return statement.executeUpdate("UPDATE teams SET teams.name = '" + team.getName() + "' WHERE id = " + team.getId()) > 0;
			
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DBException("updateTeam(): failed updaing", e);
			}
		}
		return false;
		
	}
	
	//-----------------------------------------------------------------------
	private void rollBack(Connection con) {
		try {
			con.rollback();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	private void close(AutoCloseable con) {
		try {
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void turnOnAutoCommit(Connection con) {
		try {
			con.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void addTeamForUser(Connection con, int user_id, int team_id) throws SQLException {
		PreparedStatement prst = null;
		try {
			prst = con.prepareStatement("INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)");

			prst.setInt(1, user_id);
			prst.setInt(2, team_id);
			prst.executeUpdate();
			
		} finally {
			close(prst);
		}

	}
	
	private void deleteUser(Connection con, int user_id) throws SQLException {
		PreparedStatement prst = null;
		try {
			prst = con.prepareStatement("DELETE FROM users WHERE id = ?");

			prst.setInt(1, user_id);
			prst.executeUpdate();
			
		} finally {
			close(prst);
		}

	}

}
*/